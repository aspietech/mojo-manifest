/*
 * Copyright (c) 2019, Dmitry Novikov <cat@aspie.ru>
 * Copyright (c) 2019, The Aspie Project (http://aspie.tech/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tech.aspie.mojo.manifest;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.apache.maven.plugins.annotations.Parameter;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class ManifestSection {
	@Parameter(required = true)
	private String name = null;

	@Parameter
	private Map<String, String> manifestEntries = null;

	public void setName(final String name) {
		Objects.requireNonNull(name, "name == null");
		this.name = name;
	}

	public Optional<String> getName() {
		return Optional.ofNullable(name);
	}

	public void setManifestEntries(final Map<String, String> manifestEntries) {
		Objects.requireNonNull(manifestEntries, "manifestEntries == null");
		this.manifestEntries = manifestEntries;
	}

	public Optional<Map<String, String>> getManifestEntries() {
		return Optional.ofNullable(manifestEntries);
	}
}
