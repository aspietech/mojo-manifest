/*
 * Copyright (c) 2019, Dmitry Novikov <cat@aspie.ru>
 * Copyright (c) 2019, The Aspie Project (http://aspie.tech/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tech.aspie.mojo.manifest;

import java.util.Map;
import java.util.Objects;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.Manifest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.OverConstrainedVersionException;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;

/**
 * Manifest builder.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public final class ManifestBuilder {
	public static final String MANIFEST_VERSION = "1.0";
	public static final String CLASSPATH_DELIMITER = " ";

	public static final class CustomName {
		public static final Name NAME = new Name("Name");
		public static final Name CREATED_BY = new Name("Created-By");
		public static final Name BUILD_JDK = new Name("Build-Jdk");
	}

	private final MavenSession session;

	private final MavenProject project;

	private final Manifest manifest;

	private final Attributes mainAttributes;

	public ManifestBuilder(
			final MavenSession session,
			final File manifestFile)
			throws MojoExecutionException
	{
		this.session = session;
		this.project = session.getCurrentProject();

		manifest = loadManifest(manifestFile);
		mainAttributes = manifest.getMainAttributes();
	}

	private Manifest loadManifest(final File manifestFile)
			throws MojoExecutionException
	{
		try (final var inputStream = new FileInputStream(manifestFile)) {
			return new Manifest(inputStream);
		} catch (final FileNotFoundException e) {
			return new Manifest();
		} catch (final IOException e) {
			throw new MojoExecutionException("Unable to read manifest archetype", e);
		}
	}

	public void configure(
			final ManifestConfiguration configuration)
			throws MojoExecutionException
	{
		Objects.requireNonNull(session, "session == null");
		Objects.requireNonNull(configuration, "configuration == null");

		addCommonEntries(configuration);

		if (configuration.shouldAddDefaultSpecificationEntries())
			addSpecificationEntries();

		if (configuration.shouldAddDefaultImplementationEntries())
			addImplementationEntries();
	}

	private void addCommonEntries(
			final ManifestConfiguration configuration)
	{
		Objects.requireNonNull(configuration, "configuration == null");

		var mavenSignature = "Apache Maven";

		final var mavenVersion = session.getSystemProperties().getProperty("maven.version");
		if (mavenVersion != null)
			mavenSignature = String.format("%s %s", mavenSignature, mavenVersion);

		addAttribute(Name.MANIFEST_VERSION, MANIFEST_VERSION);
		addAttribute(CustomName.CREATED_BY, mavenSignature);
		addAttribute(CustomName.BUILD_JDK, System.getProperty("java.version"));

		configuration.getMainClass().ifPresent(
				mainClass -> addAttribute(Name.MAIN_CLASS, mainClass));
		configuration.getPackageName().ifPresent(
				packageName -> addAttribute(CustomName.NAME, packageName));
	}

	private void addSpecificationEntries()
			throws MojoExecutionException
	{
		final ArtifactVersion selectedVersion;
		try {
			selectedVersion = project.getArtifact().getSelectedVersion();
		} catch (final OverConstrainedVersionException e) {
			throw new MojoExecutionException("Over-constrainted project artifact version", e);
		}

		addAttribute(Name.SPECIFICATION_TITLE, project.getName(), false);
		addAttribute(Name.SPECIFICATION_VERSION, String.format(
				"%s.%s", selectedVersion.getMajorVersion(), selectedVersion.getMinorVersion()), false);

		final var organization = project.getOrganization();
		if (organization != null)
			addAttribute(Name.SPECIFICATION_VENDOR, organization.getName(), false);
	}

	@SuppressWarnings("deprecation")
	private void addImplementationEntries() {
		Objects.requireNonNull(project, "project == null");

		addAttribute(Name.IMPLEMENTATION_TITLE, project.getName(), false);
		addAttribute(Name.IMPLEMENTATION_VERSION, project.getVersion(), false);
		addAttribute(Name.IMPLEMENTATION_VENDOR_ID, project.getGroupId(), false);

		final var organization = project.getOrganization();
		if (organization != null) {
			addAttribute(Name.IMPLEMENTATION_VENDOR, organization.getName(), false);
			addAttribute(Name.IMPLEMENTATION_URL, organization.getUrl(), false);
		}
	}

	public void addEntries(final Map<String, String> entries) {
		Objects.requireNonNull(entries, "entries == null");

		if (!entries.isEmpty())
			entries.forEach((key, value) -> addAttribute(new Name(key), value));
	}

	public void addSection(final ManifestSection section) throws MojoExecutionException {
		Objects.requireNonNull(section, "section == null");

		final var name = section.getName()
				.orElseThrow(() -> new MojoExecutionException("Missing section name"));
		final var entries = section.getManifestEntries()
				.orElseThrow(() -> new MojoExecutionException("Missing section entries"));

		if (!entries.isEmpty()) {
			final var sectionAttributes = manifest
					.getEntries()
					.computeIfAbsent(name, key -> new Attributes());

			entries.forEach(sectionAttributes::putValue);
		}
	}

	private void addAttribute(final Name key, final String value) {
		Objects.requireNonNull(key, "key == null");

		addAttribute(key, value, true);
	}

	private void addAttribute(final Name key, final String value, final boolean replace) {
		Objects.requireNonNull(key, "key == null");

		if (key.equals(Name.CLASS_PATH)) {
			final var classPath = (String) mainAttributes.get(key);

			if (classPath != null) {
				addAttribute(key, String.join(CLASSPATH_DELIMITER, classPath, value));
				return;
			}
		}

		addAttribute(mainAttributes, key, value, replace);
	}

	private void addAttribute(
			final Attributes attributes,
			final Name key,
			final String value)
	{
		Objects.requireNonNull(attributes, "attributes == null");
		Objects.requireNonNull(key, "key == null");

		addAttribute(attributes, key, value, true);
	}

	private void addAttribute(
			final Attributes attributes,
			final Name key,
			final String value,
			final boolean replace)
	{
		Objects.requireNonNull(attributes, "attributes == null");
		Objects.requireNonNull(key, "key == null");

		if (value == null)
			return;

		final var trimmedValue = value.trim();
		if (trimmedValue.isEmpty())
			return;

		if (!replace && attributes.containsKey(key))
			return;

		attributes.put(key, trimmedValue);
	}

	public void build(final File outputFile)
			throws MojoExecutionException
	{
		Objects.requireNonNull(outputFile, "outputFile == null");

		createOutputDirectory(outputFile);

		try (final var outputStream = new FileOutputStream(outputFile)) {
			manifest.write(outputStream);
			outputStream.flush();
		} catch (final IOException e) {
			throw new MojoExecutionException("Unable to write manifest file", e);
		}
	}

	private static void createOutputDirectory(final File outputFile)
			throws MojoExecutionException
	{
		Objects.requireNonNull(outputFile, "outputFile == null");

		final var path = outputFile.toPath().getParent();
		if (Files.exists(path)) {
			if (!Files.isDirectory(path)) {
				throw new MojoExecutionException(
						String.format("Not a directory: %s", path.toString()));
			}

			return;
		}

		try {
			Files.createDirectories(path);
		} catch (final IOException e) {
			throw new MojoExecutionException(
					String.format("Can't create output directory: %s", path.toString()), e);
		}
	}
}
