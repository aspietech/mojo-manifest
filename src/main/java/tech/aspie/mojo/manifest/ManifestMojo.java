/*
 * Copyright (c) 2019, Dmitry Novikov <cat@aspie.ru>
 * Copyright (c) 2019, The Aspie Project <http://aspie.tech/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tech.aspie.mojo.manifest;

import java.util.List;
import java.util.Map;

import java.io.File;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "manifest", defaultPhase = LifecyclePhase.COMPILE)
@SuppressWarnings({"WeakerAccess", "unused"})
public final class ManifestMojo extends AbstractMojo {
	@Parameter(defaultValue = "${session}", readonly = true)
	private MavenSession session;

	@Parameter(defaultValue = "${project.build.outputDirectory}/META-INF/MANIFEST.MF")
	private File outputFile;

	@Parameter(defaultValue = "${project.basedir}/src/main/resources/META-INF/MANIFEST.MF")
	private File manifestFile;

	@Parameter
	private ManifestConfiguration manifest;

	@Parameter
	private Map<String, String> manifestEntries;

	@Parameter
	private List<ManifestSection> manifestSections;

	@Override
	public void execute() throws MojoExecutionException {
		if (manifestFile.exists())
			getLog().info(String.format("Using %s as archetype", manifestFile.getAbsolutePath()));
		else
			getLog().info("Manifest archetype doesn't exists, creating from scratch");

		getLog().info(String.format("Writing manifest to %s", outputFile.getAbsolutePath()));

		if (manifest == null)
			manifest = new ManifestConfiguration();

		try {
			final ManifestBuilder builder;

			builder = new ManifestBuilder(session, manifestFile);
			builder.configure(manifest);

			if (manifestEntries != null)
				builder.addEntries(manifestEntries);

			if (manifestSections != null) {
				try {
					manifestSections.forEach(section -> {
						try {
							builder.addSection(section);
						} catch (final MojoExecutionException e) {
							throw new RuntimeException(e);
						}
					});
				} catch (final RuntimeException e) {
					if (e.getCause() instanceof MojoExecutionException)
						throw (MojoExecutionException) e.getCause();
					throw e;
				}
			}

			builder.build(outputFile);
		} catch (final MojoExecutionException e) {
			getLog().error(e.getMessage());

			if (e.getCause() != null)
				getLog().error(e.getCause().getMessage());

			throw e;
		}
	}
}
